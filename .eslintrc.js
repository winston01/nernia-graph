/* eslint sort-keys: "error" */
module.exports = {
  extends: "eslint:recommended",
  parserOptions: {
    project: ["./tsconfig.json", "./tsconfig.eslint.json"],
    sourceType: "module",
  },
  plugins: ["node"],
  root: true,
  rules: {
  },
};
